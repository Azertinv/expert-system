use std::cell::Cell;
use std::collections::HashMap;
use value::Value;
use std::process;

#[derive(PartialEq, Eq, Debug, Clone)]
pub enum Exp {
	Not(Box<Exp>),
	And(Box<Exp>, Box<Exp>),
	Or(Box<Exp>, Box<Exp>),
	Xor(Box<Exp>, Box<Exp>),
	Fact(String),
}

impl Exp {
	pub fn not(exp: Exp) -> Exp { Exp::Not(Box::new(exp)) }
	pub fn and(lhs: Exp, rhs: Exp) -> Exp { Exp::And(Box::new(lhs), Box::new(rhs)) }
	pub fn or(lhs: Exp, rhs: Exp) -> Exp { Exp::Or(Box::new(lhs), Box::new(rhs)) }
	pub fn xor(lhs: Exp, rhs: Exp) -> Exp { Exp::Xor(Box::new(lhs), Box::new(rhs)) }
	pub fn fact(id: & str) -> Exp { Exp::Fact(id.to_string()) }

	fn facts_helper(lhs: & Exp, rhs: & Exp) -> Vec<String> {
		let mut ret = lhs.facts();
		ret.append(&mut rhs.facts());
		ret
	}

	pub fn facts(&self) -> Vec<String> {
		match *self {
			Exp::Not(ref exp) => exp.facts(),
			Exp::Or(ref lhs, ref rhs) |
			Exp::And(ref lhs, ref rhs) |
			Exp::Xor(ref lhs, ref rhs) => Exp::facts_helper(lhs, rhs),
			Exp::Fact(ref id) => vec![id.clone()],
		}
	}

	pub fn propagate(&self, facts: & HashMap<String, Cell<Value>>, value: Value) {
		match *self {
			Exp::And(ref lhs, ref rhs) => {
				lhs.propagate(facts, value);
				rhs.propagate(facts, value);
			},
			Exp::Not(ref exp) => {
				exp.propagate(facts, !value);
			},
			Exp::Fact(ref id) => {
				let c = facts.get(id).unwrap();
				if c.get() != value { match c.get() { //BUGGY
					Value::Default | Value::Undefined | Value::True => c.set(value),
					Value::False => {
						println!("logic error: {:?}, {:?}", self, facts);
						process::exit(0);
					},
				}}
			},
			_ => panic!(),
		}
	}

	pub fn eval(&self, facts: & HashMap<String, Cell<Value>>) -> Value {
		match *self {
			Exp::Not(ref exp) => !exp.eval(facts),
			Exp::Or(ref lhs, ref rhs) => lhs.eval(facts) | rhs.eval(facts),
			Exp::And(ref lhs, ref rhs) => lhs.eval(facts) & rhs.eval(facts),
			Exp::Xor(ref lhs, ref rhs) => lhs.eval(facts) ^ rhs.eval(facts),
			Exp::Fact(ref id) => match facts.get(id) {
				Some(value_cell) => match value_cell.get() {
					Value::Default => Value::False,
					default => default,
				},
				None => Value::Default,
			},
		}
	}
}
