#[macro_use]
extern crate nom;
extern crate linefeed;
mod value;
mod expression;
mod rule;
mod parser;

use std::io::{Read};
use std::env;
use std::rc::Rc;
use std::collections::{HashMap};
use std::cell::Cell;
use std::fs::File;
use value::Value;
use rule::{Rule};
use parser::{parse_rule, parse_truth, parse_querie};
use linefeed::Reader;

#[derive(Debug)]
struct ExpertSystem {
	rules: Vec<Rc<Rule>>,
	facts: HashMap<String, Cell<Value>>,
	queries: Vec<String>,
	fact_ref: HashMap<String, Vec<Rc<Rule>>>,
	traced: Vec<String>,
}

impl ExpertSystem {
	pub fn new() -> ExpertSystem {
		ExpertSystem {
			rules: vec![],
			facts: HashMap::new(),
			queries: vec![],
			fact_ref: HashMap::new(),
			traced: vec![],
		}
	}

	pub fn load(&mut self, content: String){
		let mut tmp_truth = Vec::new();
		for line in content.lines() {
			let sanitized = match sanitize(line) {
				Ok(sanitized) => sanitized,
				Err(err) => {
					println!("{}", err);
					std::process::exit(0);
				},
			};
			if sanitized.is_empty() {
				continue ;
			}
			if let Ok(rule) = parse_rule(&sanitized) {
				if !rule.sane() {
					println!("unsane rule: \"{}\"", line);
					std::process::exit(0);
				}
				self.rules.push(Rc::new(rule));
			}
			else if let Ok(truth) = parse_truth(&sanitized) {
				tmp_truth = truth;
			}
			else if let Ok(querie) = parse_querie(&sanitized) {
				self.queries = querie;
			}
			else {
				println!("syntax error: {}", line);
				std::process::exit(0);
			}
		}
		for rule in &self.rules {
			for fact in rule.facts() {
				self.facts.entry(fact).or_insert(Cell::new(Value::Default));
			}
			for fact in rule.rhs_facts() {
				self.fact_ref.entry(fact).or_insert(vec![]).push(rule.clone());
			}
		}
		for truth in tmp_truth {
			self.facts.entry(truth).or_insert(Cell::new(Value::True)).set(Value::True);
		}
	}

	fn trace(&mut self, fact: String, fact_ref: & HashMap<String, Vec<Rc<Rule>>>) {
		if self.traced.contains(&fact)	{
			return;
		}
		self.traced.push(fact.clone());
		if let Some(rules) = fact_ref.get(&fact) {
			for rule in rules {
				for fact in rule.lhs_facts() {
					self.trace(fact, fact_ref);
				}
				rule.eval(&self.facts);
			}
		}
	}

	pub fn eval(&mut self) {
		let fact_ref = self.fact_ref.clone();
		for querie in &self.queries.clone() {
			if self.traced.contains(querie)	{
				continue;
			}
			self.trace(querie.clone(), &fact_ref);
		}
	}

	pub fn interactive(&mut self) {
		let mut reader = Reader::new("expert-system").unwrap();
		reader.set_prompt("?> ");
		while let Ok(Some(input)) = reader.read_line() {
			let mut words = input.split_whitespace();
			if let Some(cmd) = words.next() { match cmd{
				"help" => {
					println!("help\t\tprint this help");
					println!("load <filepath>\tload expert system from file");
					println!("eval\t\tevaluate the current expert system");
					println!("reset\t\treset the fact and rule database");
					println!("facts\t\tprint the facts and their values");
					println!("rules\t\tprint the current rules WIP");
					println!("q|quit|exit\texit interactive mode");
				},
				"load" => { if let Some(filepath) = words.next() {
					let mut file = match File::open(&filepath) {
						Ok(file) => file,
						Err(err) => {
							println!("{}: {}", err, filepath);
							continue;
						},
					};
					let mut content = String::new();
					if let Err(e) = file.read_to_string(&mut content) {
						println!("{}: {}", e, filepath);
						continue ;
					}
					self.rules = vec![];

					self.facts = HashMap::new();
					self.queries = vec![];
					self.fact_ref = HashMap::new();
					self.traced = vec![];
					self.load("".to_string());
					self.load(content);
				}},
				"reset" => {
					self.rules = vec![];
					self.facts = HashMap::new();
					self.queries = vec![];
					self.fact_ref = HashMap::new();
					self.traced = vec![];
					self.load("".to_string());
				},
				"debug" => {
					println!("{:?}", self);
				},
				"rules" => {
					println!("not yet implemented");
				}
				"facts" => {
					for (fact, value) in &self.facts {
						println!("{}: {:?}", fact, value.get());
					}
				},
				"eval" => {
					self.eval();
					self.print_result();
					self.traced.clear();
				},
				"q"|"quit"|"exit" => {break;},
				n => {println!("\"{}\" unknown command\ntry help", n);},
			}}
		}
	}

	pub fn print_result(&self) {
		for querie in &self.queries {
			if let Some(value) = self.facts.get(querie) {
				println!("{}: {:?}", querie, value.get());
			}
			else {
				println!("{}: {:?}", querie, Value::Default);
			}
		}
	}
}

fn sanitize(line: & str) -> Result<String, String> {
	let result = line.chars().filter(|c| !c.is_whitespace()).collect::<String>();
	if result.chars().all(|c| ((c.is_alphabetic() && c.is_uppercase()) || "=>+|!()^?\n".chars().any(|d| d == c))) {
		Ok(result)
	}
	else {
		Err(format!("invalid character at line: \"{}\"", line))
	}
}

fn usage() {
	println!("usage: expert_system [filepath]");
}

fn main() {
	if env::args().count() == 2 {
		let filepath = env::args().nth(1).unwrap();
		let mut file = match File::open(&filepath) {
			Ok(file) => file,
			Err(err) => {println!("{}: {}", err, filepath); return ;},
		};

		let mut content = String::new();
		if let Err(e) = file.read_to_string(&mut content) {
			println!("{}: {}", e, filepath);
			return ;
		}

		let mut system = ExpertSystem::new();
		system.load(content);
		system.eval();
		system.print_result();
	}
	else if env::args().count() == 1 {
		let mut system = ExpertSystem::new();
		system.load("".to_string());
		system.interactive();
	}
	else {
		usage();
	}
}
