use expression::Exp;
use rule::{Rule};
use nom::IResult;
use std::str::{from_utf8};

named!(parens<Exp>, alt!(
	delimited!(char!('('), summand_xor, char!(')'))
));

named!(fact<Exp>, alt!(
		map!(chain!(tag!("!") ~ ret: fact, || {return ret}), Exp::not)
	|	parens
	|	map!(map_res!(take!(1), from_utf8), Exp::fact)
));

named!(summand_and<Exp>,
	chain!(
		mut lhs: fact ~
		many0!(tap!(rhs: preceded!(tag!("+"), fact) => lhs = Exp::and(lhs, rhs.clone()))),
		|| {return lhs}
	)
);

named!(summand_or<Exp>,
	chain!(
		mut lhs: summand_and ~
		many0!(tap!(rhs: preceded!(tag!("|"), summand_and) => lhs = Exp::or(lhs, rhs.clone()))),
		|| {return lhs}
	)
);

named!(summand_xor<Exp>,
	chain!(
		mut lhs: summand_or ~
		many0!(tap!(rhs: preceded!(tag!("^"), summand_or) => lhs = Exp::xor(lhs, rhs.clone()))),
		|| {return lhs}
	)
);

named!(rule<(Exp, &[u8], Exp)>,
	tuple!(
		summand_xor,
		tag!("=>"),
		summand_xor
	)
);

pub fn parse_rule(line: & str) -> Result<Rule, String> {
	match rule(line.as_bytes()) {
		IResult::Done(_, (lhs, _, rhs)) => Ok(Rule::new(lhs, rhs)),
		_ => Err(format!("syntax error: {}", line)),
	}
}

named!(truths< Vec<String> >, chain!(
	tag!("=") ~
	ret: many0!( map!(map_res!(take!(1), from_utf8), str::to_string )),
	|| {return ret;}
));

pub fn parse_truth(line: & str) -> Result<Vec<String>, String> {
	match truths(line.as_bytes()) {
		IResult::Done(_, t) => Ok(t),
		_ => Err(format!("syntax error: {}", line)),
	}
}

named!(queries< Vec<String> >, chain!(
	tag!("?") ~
	ret: many1!( map!(map_res!(take!(1), from_utf8), str::to_string )),
	|| {return ret;}
));

pub fn parse_querie(line: & str) -> Result<Vec<String>, String> {
	match queries(line.as_bytes()) {
		IResult::Done(_, t) => Ok(t),
		_ => Err(format!("syntax error: {}", line)),
	}
}
