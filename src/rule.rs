use std::collections::HashMap;
use std::cell::Cell;
use expression::{Exp};
use value::{Value};

#[derive(PartialEq, Eq, Debug)]
pub struct Rule {
	lhs: Exp,
	rhs: Exp,
}

impl Rule {
	pub fn new(lhs: Exp, rhs: Exp) -> Rule {
		Rule {lhs: lhs, rhs: rhs}
	}

	pub fn rhs_facts(&self) -> Vec<String> {
		self.rhs.facts()
	}

	pub fn lhs_facts(&self) -> Vec<String> {
		self.lhs.facts()
	}

	pub fn facts(&self) -> Vec<String> {
		let mut ret = self.lhs.facts();
		ret.append(&mut self.rhs.facts());
		ret
	}

	pub fn eval(&self, facts: & HashMap<String, Cell<Value>>) {
		let result = self.lhs.eval(facts);
		if result == Value::True || result == Value::Undefined {
			self.rhs.propagate(facts, result);
		}
	}

	pub fn sane(&self) -> bool {
		fn sane_helper(exp: & Exp) -> bool {
			match *exp {
				Exp::And(ref lhs, ref rhs) => sane_helper(lhs) && sane_helper(rhs),
				Exp::Not(ref exp) => match *exp.as_ref() {Exp::Fact(_) => true, _ => false,},
				Exp::Fact(_) => true,
				_ => false,
			}
		}
		sane_helper(&self.rhs)
	}
}
