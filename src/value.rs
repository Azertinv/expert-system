use std::ops::{Not, BitOr, BitXor, BitAnd};

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum Value {
	True,
	False,
	Undefined,
	Default,
}

impl Not for Value {
	type Output = Value;
	fn not(self) -> Value {
		match self {
			Value::Undefined => Value::Undefined,
			Value::Default | Value::False => Value::True,
			_ => Value::False,
		}
	}
}

impl BitOr for Value {
	type Output = Value;
	fn bitor(self, rhs: Value) -> Value {
		if self == Value::True || rhs == Value::True {
			Value::True
		}
		else if self == Value::Undefined || rhs == Value::Undefined {
			Value::Undefined
		}
		else {
			Value::False
		}
	}
}

impl BitAnd for Value {
	type Output = Value;
	fn bitand(self, rhs: Value) -> Value {
		if self == Value::True && rhs == Value::True {
			Value::True
		}
		else if self == Value::Undefined && rhs == Value::Undefined ||
			self == Value::True && rhs == Value::Undefined ||
			self == Value::Undefined && rhs == Value::True {
			Value::Undefined
		}
		else {
			Value::False
		}
	}
}

impl BitXor for Value {
	type Output = Value;
	fn bitxor(self, rhs: Value) -> Value {
		if self == Value::Undefined || rhs == Value::Undefined {
			Value::Undefined
		}
		else if self == !rhs || !self == rhs{
			Value::True
		}
		else {
			Value::False
		}
	}
}
